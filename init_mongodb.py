#!/usr/bin/env python

from utils import mongo_helper

mongo_helper.get_tweets().drop()
mongo_helper.get_users().drop()
