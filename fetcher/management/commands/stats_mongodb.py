from django.core.management.base import BaseCommand, CommandError
import pymongo
from utils import mongo_helper
from utils import heading


PRINT_TOP_GROUP = 10


def print_stats():
    """Print several stats of mongodb"""

    tweets = mongo_helper.get_tweets()
    users = mongo_helper.get_users()

    # tweets
    heading('Tweets')
    tweets_count = tweets.count()
    print "- tweets: %d" % tweets_count
    ratio_to_tweets = lambda x: x * 100.0 / tweets_count

    if not tweets_count:
        return

    oldest_tweet = tweets.find(
        fields=['created_at'], limit=1,
        sort=[('created_at', pymongo.ASCENDING)])[0]
    print "- oldest tweet at: %s" % oldest_tweet['created_at']

    latest_tweet = tweets.find(
        fields=['created_at'], limit=1,
        sort=[('created_at', pymongo.DESCENDING)])[0]
    print "- latest tweet at: %s" % latest_tweet['created_at']

    # tweet metadata
    heading('Tweets metadata')

    fields = [
        ['geo', {'$ne': None}],
        ['place', {'$ne': None}],
        ['favorited', True],
        ['retweeted', True]]
    for f, q in fields:
        matched_count = tweets.find({f: q}).count()
        print "- tweets with %s: %d (%2.02f%%)" % (
            f, matched_count, ratio_to_tweets(matched_count))

    # check integrity
    tweet_with_user_id = tweets.find({'user.id': {'$ne': None}}).count()
    print "- tweets with user_id: %d (%2.02f%%)" % (
        tweet_with_user_id, ratio_to_tweets(tweet_with_user_id))

    # tweets by language
    s = lambda x, y: cmp(y['count'], x['count'])

    data = [
        ['Tweets by language (based on user\'s profile)', {'user.lang': True}],
        ['Tweets by guess-language', {'guessed_lang': True}],
        ['Tweets by user_id', {'user.id': True}]]

    for heading_str, key in data:
        heading(heading_str)

        # FIXME group() can't handle more than 20000 unique keys
        tweets_grouped = tweets.group(
            key=key,
            condition={},
            initial={'count': 0},
            reduce='function(obj,prev) { prev.count += 1; }')
        tweets_grouped.sort(s)
        column = key.keys()[0]
        for pair in tweets_grouped[0:PRINT_TOP_GROUP]:
            print "%5s\t%d" % (pair[column], pair['count'])

    # user stats
    heading('Users')

    users_count = users.count()
    print "- users: %d" % users_count
    ratio_to_users = lambda x: x * 100.0 / users_count

    users_count_by_tweets = len(tweets.distinct("user.id"))
    if users_count != users_count_by_tweets:
        print "the number of user records != distinct users in tweets(%d)" % (
            users_count_by_tweets)

    geo_enabled_users_count = len(tweets.find({'user.geo_enabled': True}).
        distinct("user.id"))
    print "- geo enabled users: %d (%2.02f%%)" % (
        geo_enabled_users_count, ratio_to_users(geo_enabled_users_count))

    data = [
        ['Language by user', {'lang': True}],
        ['Language of description by guess-language',
            {'description_guessed_lang': True}]]

    for heading_str, key in data:
        heading(heading_str)

        users_grouped = users.group(
            key=key,
            condition={},
            initial={'count': 0},
            reduce='function(obj,prev) { prev.count += 1; }')
        users_grouped.sort(s)
        column = key.keys()[0]
        for pair in users_grouped[0:PRINT_TOP_GROUP]:
            print "%5s\t%d" % (pair[column], pair['count'])

    # event stats
    heading('Events')

    events = mongo_helper.get_events()

    events_count = events.find().count()
    print "- events: %d (%2.02f of tweets)" % (
        events_count, ratio_to_tweets(events_count))
    ratio_to_events = lambda x: x * 100.0 / events_count

    delete_events_count = events.find({'delete': {'$exists': True}}).count()
    print "- delete events: %d (%2.02f%%)" % (
        delete_events_count, ratio_to_events(delete_events_count))

    other_events_count = events.find({'delete': {'$exists': False}}).count()
    print "- other events: %d (%2.02f%%)" % (
        other_events_count, ratio_to_events(other_events_count))


class Command(BaseCommand):
    args = 'user_name'
    help = 'Fetch all tweets from given user\'s stream'

    def handle(self, *args, **options):
        print_stats()
