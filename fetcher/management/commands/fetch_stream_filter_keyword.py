from django.core.management.base import BaseCommand, CommandError
import fetcher


class Command(BaseCommand):
    args = 'keywords'
    help = 'Fetch all tweets from stream for given keyowrds'

    def handle(self, *args, **options):
        if not len(args) == 1:
            print self.help
            print "Required args: %s" % self.args
            return
        fetcher.fetch_stream_filter_keyword(args[0])
