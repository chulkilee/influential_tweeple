from django.core.management.base import BaseCommand, CommandError
import fetcher


class Command(BaseCommand):
    args = 'user_name'
    help = 'Fetch all accessible tweets from given user'

    def handle(self, *args, **options):
        if not len(args) == 1:
            print self.help
            print "Required args: %s" % self.args
            return
        user = fetcher.get_user_by_screen_name(args[0])
        fetcher.fetch_statuses_user_timeline_all(user['id'])
