from django.core.management.base import BaseCommand, CommandError
import fetcher


class Command(BaseCommand):
    args = 'user_name'
    help = 'Fetch all tweets from given user\'s stream'

    def handle(self, *args, **options):
        if not len(args) == 1:
            print self.help
            print "Required args: %s" % self.args
            return
        fetcher.fetch_stream_filter_follow(args[0])
