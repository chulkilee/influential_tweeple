from django.core.management.base import BaseCommand, CommandError
import fetcher


class Command(BaseCommand):
    args = ''
    help = 'Fetch all tweets from stream'

    def handle(self, *args, **options):
        fetcher.fetch_stream_sample()
