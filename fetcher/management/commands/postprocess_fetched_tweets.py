from django.core.management.base import BaseCommand, CommandError
import sys
from utils import heading
from utils import mongo_helper
from guess_language import guessLanguage
from guess_language.guess_language import UNKNOWN


def process_tweets():
    heading('Process tweets')
    tweets = mongo_helper.get_tweets()
    count = 0
    for tweet in tweets.find():
        # print progress
        if count % 100 == 0:
            print count,
            sys.stdout.flush()

        updated = False
        # Add guessed language
        if not 'guessed_lang' in tweet:
            tweet['guessed_lang'] = guessLanguage(tweet['text'])
            updated = True
        if updated:
            tweets.save(tweet)

        count += 1


def process_users():
    heading('Process users')
    users = mongo_helper.get_users()
    count = 0
    for user in users.find():
        # print progress
        if count % 100 == 0:
            print count,
            sys.stdout.flush()

        updated = False
        # Add guessed language
        f = 'description_guessed_lang'
        if not f in user:
            user[f] = guessLanguage(user['description'])
            updated = True
        if updated:
            users.save(user)

        count += 1


class Command(BaseCommand):
    args = ''
    help = 'Process fetched data'

    def handle(self, *args, **options):
        process_tweets()
        process_users()
