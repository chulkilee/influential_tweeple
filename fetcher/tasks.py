from celery.task import task
from analysis import keyword_fetcher
from analysis.importance import get_importance
import fetcher
from utils import twitter_helper
from utils import mongo_helper


@task
def update_home_timeline(user_oauth_tokens, user_id):
    print "### (Stage 1/2) Fetching tweets for user %s" % str(user_id)
    oauth_token, oauth_secret = user_oauth_tokens
    api_user = twitter_helper.get_api(oauth_token, oauth_secret)
    fetcher.fetch_statuses_home_timeline_all(api_user, user_id)


@task
def update_user_timeline(user_id):
    print '### (Stage 2/2) Fetching tweets for user %s' % str(user_id)
    fetcher.fetch_statuses_user_timeline_all(user_id)


@task
def update_network(user_id):
    print '### Updating tweeple network for user %s' % str(user_id)
    fetcher.fetch_network(user_id)


@task
def update_entities(screen_name, user_id):
    print '### Analyzing entities in tweets for user %s' % str(user_id)
    try:
        return keyword_fetcher.extract_entities(screen_name)
    except Exception, e:
        print e
        return False


@task
def update_importance(user_id):
    print '### Updating tweet importance for user %s' % str(user_id)
    tweets = mongo_helper.get_tweets()
    count = 0
    q = {'r.home_timeline': user_id, 'r.score': {'$exists': False}}
    for t in tweets.find(q):
        try:
            print t['text']
            score = get_importance(t['text'], user_id)
            print 'Important: ' + str(score)
        except Exception, e:
            print e
            score = None
        if 'r' not in t:
            t['r'] = {}
        t['r']['score'] = score
        tweets.save(t)
        count += 1
    return count
