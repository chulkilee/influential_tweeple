# Authors

- Arthur Suermondt
- Chulki Lee
- Ram Joshi

# Requirements

- Python 2.6 or 2.7
- Python packages:
  - [twitter](http://pypi.python.org/pypi/twitter)
  - [Django](http://www.djangoproject.com)
  - [Celery](https://github.com/ask/celery/)
  - [pymongo](http://pypi.python.org/pypi/pymongo/)
  - [guess-language](http://code.google.com/p/guess-language/)
  - [nltk](http://www.nltk.org/)
  - simplejson
  - [lxml](http://lxml.de)
  - oauth2
  - [AlchemyAPI](http://www.alchemyapi.com/): Python library is included in utils module.
- [MongoDB](http://www.mongodb.org/)

# Configuration

## Install python packages

All requirements are listed in requirements.txt, which can be used for pip command.

    pip -r requirements.txt

## Create a Twitter app

To run Influential Tweeple, you need to create a Twitter app. You can create it at [Create an application](https://dev.twitter.com/apps/new).

You must set *WebSite* and *Callback URL* properly to make Influential Tweeple working. If you run the server at localhost at 8000 port, you must fill like as followings:

- WebSite: http://localhost:8000
- Callback URL: http://127.0.0.1:8000/twitter/authenticated/

Once you created an app, you can see the list at [My applications](https://dev.twitter.com/apps/). Influential Tweeple requires *Access token* and *Access token secret*. You need to set them in config.yml

## Update configuration

First, copy example files.

    cp config.yml.example config.yml
    cp celeryconfig.py.example celeryconfig.py

Then update configuration files.

- config.yml
  - Set twitter oauth configuration, described in previous section.
  - Set alchemy api key. You can get it at [AlchemyAPI](http://www.alchemyapi.com/)
  - Set MySQL configuration
  - Edit mongodb configuration
- celeryconfig.py
  - Edit MongoDB configuration

## Prepare database

MongoDB does not need create or migration, so only MySQL database migration is required. It is done by Django syncdb task.

    # run Django syncdb
    ./manage.py syncdb

## Run all daemons
    # ensure running daemons at the root of the influential
    cd path-to-influential

    # run MongoDB server if needed
    mongod --dbpath /path-to-mongodb-dbpath

    # run Celery
    celeryd


    # run Django server
    ./manage.py runserver

# How to use

First, sign up Influential Tweeple(the website). Then you can link your account at the website to your twitter account.

Once you grant permissions to Influential Tweeple application, Influential Tweeple can fetch tweets and process them. To do so, click *Update* link at the top navigation.


# Known Issues
- Does not deal with screen_name changes
- Score is not personalized

# To Do
- weighted score
- importance of users
- cron tasks
- refactoring
