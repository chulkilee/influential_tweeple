import pymongo
import fetcher
from utils import mongo_helper
import time

tweets = mongo_helper.get_tweets()
users = mongo_helper.get_users()


def original_timeline(profile, limit=200):
    res = []
    q = {'r.home_timeline': profile.twitter_id_str}
    s = [('created_at', pymongo.DESCENDING)]
    for t in tweets.find(q, sort=s, limit=limit):
        t['date'] = t['created_at']  # TODO convert to Python object
        t['user'] = users.find_one({'id': t['user']['id']})
        res.append(t)
    return res


def important_timeline(profile, limit=200):
    res = []
    q = {'r.home_timeline': profile.twitter_id_str, 'r.score': True}
    s = [('created_at', pymongo.DESCENDING)]
    for t in tweets.find(q, sort=s, limit=limit):
        # FIXME if user info is missing, just add it
        t['date'] = t['created_at']  # TODO convert to Python object
        if 'screen_name' not in t['user']:
            t['user'] = fetcher.get_user_by_id_str(t['user']['id_str'])
            tweets.save(t)
        res.append(t)
    return res


def important_tweeple(profile, limit=5):
    res = []
    user = profile.user_doc()
    if 'r' not in user or 'graph' not in user['r']:
        q = {'id_str': {'$ne': profile.twitter_id_str}}
    else:
        followers = profile.user_doc()['r']['graph']['followers']
        q = {'id_str': {'$ne': profile.twitter_id_str},
            'id': {'$in': followers}}
    for u in users.find(q, limit=limit):
        res.append(u)
    return res


def suggested_tweeple(profile, limit=5):
    res = []
    user = profile.user_doc()
    if 'r' not in user or 'graph' not in user['r']:
        q = {'id_str': {'$ne': profile.twitter_id_str}}
    else:
        followers = profile.user_doc()['r']['graph']['followers']
        q = {'id_str': {'$ne': profile.twitter_id_str},
            'id': {'$nin': followers}}
    for u in users.find(q, limit=limit):
        res.append(u)

    return res
