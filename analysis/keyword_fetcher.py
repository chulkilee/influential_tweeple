import pymongo
from lxml import etree
import utils
from utils import mongo_helper
from utils import AlchemyAPI

ALCHEMY_API_KEY = utils.CONFIG['alchemy']['api_key']


def extract_entities(screen_name):
    '''
    Extracts keywords and entities with relevance scores for a user's tweets,
    using the Alchemy API.
    Results and stored in keywords and entities collections.

    Keyword results have a format like
    {
        "relevance" : 0.123,
        "text" : "music",
        "user" : {"id_str" : "112233"}
    }.

    Entity results have a format like
    {
        "relevance" : 0.123,
        "entity" : ["SoftwareDeveloper", "VentureFundedCompany"],
        "user" : {"id_str" : "112233"}
    }.
    '''
    user = mongo_helper.get_user(screen_name)
    if not user:
        return False
    id_str = user['id_str']
    alchemy = AlchemyAPI.AlchemyAPI()
    alchemy.setAPIKey(ALCHEMY_API_KEY)
    alltweets = ''.join(get_all_tweets(id_str))

    keyword_results = alchemy.TextGetRankedKeywords(alltweets)
    keywords = etree.fromstring(keyword_results)
    collection = mongo_helper.get_keywords()
    for keyword in keywords.iterfind('.//keyword'):
        k = {}
        k['user'] = {'id_str': id_str}
        k['text'] = keyword.find('text').text
        k['relevance'] = float(keyword.find('relevance').text)
        try:
            collection.save(k)
        except pymongo.errors.DuplicateKeyError, e:
            pass

    entity_results = alchemy.TextGetRankedNamedEntities(alltweets)
    entities = etree.fromstring(entity_results)
    collection = mongo_helper.get_entities()
    for entity in entities.iterfind('.//entity'):
        e = {}
        e['user'] = {'id_str': id_str}
        types = []
        for type in entity.iterfind('.//subType'):
                types.append(type.text)
        if not types:
                types.append(entity.find('type').text)
        e['entity'] = types
        e['relevance'] = float(entity.find('relevance').text)
        try:
                collection.insert(e)
        except pymongo.errors.DuplicateKeyError, e:
                pass
    return True


def get_all_tweets(id_str):
    tweets = mongo_helper.get_tweets_for_id_str(id_str)
    text = []
    for tweet in tweets:
        text.append(tweet['text'].encode('utf-8'))
    return text
