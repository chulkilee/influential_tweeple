from django.core.management.base import BaseCommand, CommandError
from analysis import keyword_fetcher


class Command(BaseCommand):
    args = 'user_name'
    help = 'Keyword analysis for a given user\'s tweets'

    def handle(self, *args, **options):
        if not len(args) == 1:
            print self.help
            print "Required args: %s" % self.args
            return

        messages = {"ok": "Done",
            "no_user": "User not found. Did you run fetch.py for this user?"}
        screen_name = args[0]

        if keyword_fetcher.extract_entities(screen_name):
            print messages["ok"]
        else:
            print messages["no_user"]
