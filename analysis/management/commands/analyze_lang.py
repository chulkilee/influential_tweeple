from django.core.management.base import BaseCommand, CommandError
from analysis import language


class Command(BaseCommand):
    args = 'user_name'
    help = 'Determine primary language for a given user'

    def handle(self, *args, **options):
        if not len(args) == 1:
            print self.help
            print "Required args: %s" % self.args
            return
        pass  # TODO
