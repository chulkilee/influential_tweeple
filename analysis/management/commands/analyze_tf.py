from django.core.management.base import BaseCommand, CommandError
from analysis import term_freq


class Command(BaseCommand):
    args = 'user_name'
    help = 'Term frequency analysis for a given user\'s tweets'

    def handle(self, *args, **options):
        if not len(args) == 1:
            print self.help
            print "Required args: %s" % self.args
            return
        pass  # TODO
