from lxml import etree
import utils
from utils import mongo_helper
from utils import AlchemyAPI

ALCHEMY_API_KEY = utils.CONFIG['alchemy']['api_key']


def get_importance(tweet_text, id_str):
    '''
    Returns boolean value True/False if a tweet is important or not
    '''
    tweet_text = tweet_text.encode('utf8')
    important = False
    user = mongo_helper.get_user_by_id_str(id_str)
    if not user:
        return False

    alchemy = AlchemyAPI.AlchemyAPI()
    alchemy.setAPIKey(ALCHEMY_API_KEY)

    keyword_results = alchemy.TextGetRankedKeywords(tweet_text)
    keywords_xml = etree.fromstring(keyword_results)
    keywords = []
    for keyword in keywords_xml.iterfind('.//keyword'):
        keywords.append(keyword.find('text').text)

    u_keywords = mongo_helper.get_keywords_for_id_str(id_str)
    user_keywords = []
    for user_keyword in u_keywords:
        user_keywords.append(user_keyword['text'].lower().strip())

    if any(k.lower().strip() in user_keywords for k in keywords):
        important = True

    if not important:
        entity_results = alchemy.TextGetRankedNamedEntities(tweet_text)
        entities_xml = etree.fromstring(entity_results)
        entities = []
        for entity in entities_xml.iterfind('.//entity'):
            for type in entity.iterfind('.//subType'):
                entities.append(type.text)
            if not entities:
                entities.append(entity.find('type').text)

        if entities:
            u_entities = mongo_helper.get_entities_for_id_str(id_str)
            user_entities = []
            for user_entity in u_entities:
                for e in user_entity['entity']:
                    user_entities.append(e.lower().strip())
            if any(e.lower().strip() in user_entities for e in entities):
                important = True

    return important
