import os
import nltk
import utils


def tokenize_document(document):
    tokens = []
    sentences = nltk.sent_tokenize(document)
    for sentence in sentences:
        words = nltk.word_tokenize(sentence)
        for word in words:
            tokens.append(word)
    return tokens


def clean_tokens(tokens):
    clean_tokens = []
    for token in tokens:
        if len(token) < 2:  # Only save words longer than 2 chars
            continue
        clean_tokens.append(token.lower())  # Always lower case
    return clean_tokens


def process_document(document):
    return clean_tokens(tokenize_document(document))


def get_term_freq(tokens):
    tf = {}
    for token in tokens:
        if token not in tf:
            tf[token] = float(tokens.count(token)) / float(len(tokens))
    return tf


# def main():
#     server = couchdb.Server('http://127.0.0.1:5984')
#     db = server['rcv1_test_100']
#
#     # Tokenize ansd compute term frequencies for all the documents in the db.
#     q = '''function(doc) { if (!('tf' in doc)) emit(doc._id, null); }'''
#     for result in db.query(q):
#         doc = db[result.key]
#         tokens = process_document(doc['text'])
#         doc['tokens'] = tokens
#         doc['tf'] = get_term_freq(tokens)
#         db[result.key] = doc
#
# if __name__ == '__main__':
#     main()
