#!/usr/bin/env python
import sys
from utils import mongo_helper
from utils import heading


def test_mongodb():
    db = None
    try:
        heading("Get connection and database")
        db = mongo_helper.get_db()
    except Exception, e:
        print "[Error]", str(e)
    else:
        print "[Success]"
    pass


if __name__ == '__main__':
    test_mongodb()
