from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User
from utils import mongo_helper as mh
from utils import twitter_helper
import fetcher


class UserProfile(models.Model):
    # This field is required.
    user = models.OneToOneField(User)

    # Other fields here
    twitter_screen_name = models.CharField(max_length=200)
    twitter_id_str = models.CharField(max_length=200)
    twitter_oauth_token = models.CharField(max_length=200)
    twitter_oauth_secret = models.CharField(max_length=200)

    def twitter_logged_in(self):
        return (self.twitter_oauth_token and self.twitter_oauth_secret)

    def twitter_oauth_tokens(self):
        return (self.twitter_oauth_token, self.twitter_oauth_secret)

    def twitter_api(self):
        return twitter_helper.get_api(
            self.twitter_oauth_token, self.twitter_oauth_secret)

    def tweets_count(self):
        return mh.get_tweets_for_id_str(self.twitter_id_str).count()

    def timeline_tweets_count(self):
        return mh.get_tweets_home_timeline(self.twitter_id_str).count()

    def friends_count(self):
        return self._graph_count('friends')

    def followers_count(self):
        return self._graph_count('followers')

    def mutuals_count(self):
        return self._graph_count('mutuals')

    def _graph_count(self, name):
        u = self.user_doc()
        if 'graph' not in u['r']:
            return None
        else:
            return len(u['r']['graph'][name])

    def user_doc(self):
        if not hasattr(self, '__user_doc'):
            self.__user_doc = fetcher.get_user_by_id_str(self.twitter_id_str)
        if 'r' not in self.__user_doc:
            self.__user_doc['r'] = {}
        return self.__user_doc


@receiver(post_save, sender=User)
def __create_user_profile(sender, instance, created, **kwargs):
    if created:
        profile, new = UserProfile.objects.get_or_create(user=instance)
