# some code are from
# https://github.com/simplegeo/python-oauth2

# Python
import oauth2 as oauth
from cgi import parse_qsl
# Django
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.views.decorators.http import require_http_methods
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.template import RequestContext
from django.core.urlresolvers import reverse
# Custom
from utils import twitter_helper
from utils import mongo_helper
from analysis import engine
from fetcher import tasks

request_token_url = 'http://twitter.com/oauth/request_token'
access_token_url = 'http://twitter.com/oauth/access_token'
authenticate_url = 'http://twitter.com/oauth/authenticate'


@login_required
def twitter_login(request):
    """Step 1: get the request token and redirect user to Twitter"""
    client = twitter_helper.get_oauth_client()

    # Step 1. Get a request token from Twitter.
    resp, content = client.request(request_token_url, 'GET')
    if resp['status'] != '200':
        raise Exception('Invalid response from Twitter.')

    # Step 2. Store the request token in a session for later use.
    request.session['request_token'] = dict(parse_qsl(content))

    # Step 3. Redirect the user to the authentication URL.
    url = "%s?oauth_token=%s" % (authenticate_url,
        request.session['request_token']['oauth_token'])

    return HttpResponseRedirect(url)


@login_required
def twitter_authenticated(request):
    """Step 2: get the access token and store info into profile"""
    # Step 1. Use the request token in the session to build a new client.
    client = twitter_helper.get_oauth_client_with_token(
        request.session['request_token']['oauth_token'],
        request.session['request_token']['oauth_token_secret'])

    # Step 2. Request the authorized access token from Twitter.
    resp, content = client.request(access_token_url, 'GET')
    if resp['status'] != '200':
        raise Exception('Invalid response from Twitter.')

    """
    This is what you'll get back from Twitter. Note that it includes the
    user's user_id and screen_name.
    {
        'oauth_token_secret': 'IcJXPiJh8be3BjDWW50uCY31chyhsMHEhqJVsphC3M',
        'user_id': '120889797',
        'oauth_token': '120889797-H5zNnM3qE0iFoTTpNEHIz3noL9FKzXiOxwtnyVOD',
        'screen_name': 'heyismysiteup'
    }
    """
    access_token = dict(parse_qsl(content))

    # Step 3. Lookup the user
    # Save our permanent token and secret for later.
    u = request.user
    profile = u.get_profile()

    profile.twitter_screen_name = access_token['screen_name']
    profile.twitter_id_str = str(access_token['user_id'])
    profile.twitter_oauth_token = access_token['oauth_token']
    profile.twitter_oauth_secret = access_token['oauth_token_secret']
    profile.save()

    return HttpResponseRedirect(reverse('home'))


@login_required
def twitter_profile(request):
    profile = request.user.get_profile()

    if not profile.twitter_logged_in():
        return HttpResponseRedirect(reverse('twitter_login'))
    else:
        api_user = profile.twitter_api()
        twitter_user = api_user.account.verify_credentials()
        return render_to_response('profile.html',
            {'twitter': twitter_user},
            context_instance=RequestContext(request))


@login_required
@require_http_methods(['GET'])
def twitter_logout(request):
    # delete saved keys
    # TODO revoke
    profile = request.user.get_profile()
    profile.twitter_screen_name = ''
    profile.twitter_id_str = ''
    profile.twitter_oauth_token = ''
    profile.twitter_oauth_secret = ''
    profile.save()

    return HttpResponseRedirect(reverse('home'))


@require_http_methods(['GET', 'POST'])
def register(request):
    # if logged in, redirect to home
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('home'))

    # if POST, process form
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            return HttpResponseRedirect(reverse('login'))
    # if GET, show form
    else:
        form = UserCreationForm()
    return render_to_response('registration/register.html', {'form': form},
        context_instance=RequestContext(request))


@login_required
@require_http_methods(['GET'])
def home(request):
    profile = request.user.get_profile()
    if not profile.twitter_logged_in():
        return render_to_response('home.html',
            context_instance=RequestContext(request))
    else:
        suggested_tweeple = engine.suggested_tweeple(profile)
        important_tweeple = engine.important_tweeple(profile)
        original_timeline = engine.original_timeline(profile)
        important_timeline = engine.important_timeline(profile)

        template_vars = {'suggested_tweeple': suggested_tweeple,
            'important_tweeple': important_tweeple,
            'original_timeline': original_timeline,
            'important_timeline': important_timeline,
        }
        return render_to_response('home.html', template_vars,
            context_instance=RequestContext(request))


@require_http_methods(['GET'])
def about(request):
    return render_to_response('about.html',
        context_instance=RequestContext(request))


@login_required
@require_http_methods(['GET'])
def stats(request):
    stats = {}  # for template
    profile = request.user.get_profile()
    # if has twitter account info
    if profile.twitter_logged_in():
        stats['tweets_self'] = profile.tweets_count()
        stats['tweets_timeline'] = profile.timeline_tweets_count()
        stats['friends'] = profile.friends_count()
        stats['followers'] = profile.followers_count()
        stats['mutuals'] = profile.mutuals_count()
    return render_to_response('stats.html',
        {'stats': stats},
        context_instance=RequestContext(request))


@login_required
def update(request):
    """Run delayed tasks"""
    # TODO run every x seconds rather than whenever requested
    profile = request.user.get_profile()
    delayed = True
    tasks.update_home_timeline.delay(
        profile.twitter_oauth_tokens(), profile.twitter_id_str)
    tasks.update_user_timeline.delay(profile.twitter_id_str)
    tasks.update_network.delay(profile.twitter_id_str)
    tasks.update_entities.delay(profile.twitter_screen_name,
        profile.twitter_id_str)
    tasks.update_importance.delay(profile.twitter_id_str)
    return render_to_response('update.html',
        context_instance=RequestContext(request))
