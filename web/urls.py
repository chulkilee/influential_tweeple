from django.conf.urls.defaults import patterns, include, url
import django.contrib.auth.views as auth_views
import views


# enable the admin:
from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',
    # main
    url(r'^/?$', views.home, name='home'),
    url(r'^about/$', views.about, name='about'),
    url(r'^stats/$', views.stats, name='stats'),

    # accounts
    url(r'^accounts/login/$', auth_views.login, name='login'),
    url(r'^accounts/logout/$', auth_views.logout,
        {'template_name': 'registration/logout.html'}, name='logout'),
    url(r'^accounts/register/$', views.register, name='register'),
    url(r'^accounts/profile/$', views.twitter_profile, name='profile'),

    # twitter authentication
    url(r'^twitter/login/?$', views.twitter_login, name='twitter_login'),
    url(r'^twitter/authenticated/?$', views.twitter_authenticated,
        name='twitter_authenticated'),
    url(r'^twitter/logout/?$', views.twitter_logout, name='twitter_logout'),

    # update
    url(r'^update/?$', views.update, name='update'),

    # enable admin documentation
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # enable the admin
    url(r'^admin/', include(admin.site.urls)),
)
