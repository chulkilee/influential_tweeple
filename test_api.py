#!/usr/bin/env python
import sys
from utils import twitter_helper
from utils import pp


def test_api(screen_name):
    """Test twitter api and stream"""
    api = twitter_helper.get_api_fetcher()

    pp('Twitter screen name', screen_name)

    u = api.users.show(screen_name=screen_name)
    pp('User', u)
    user_id = u['id']

    follower_ids = api.followers.ids(screen_name=screen_name)
    pp('Follower IDs', follower_ids)

    following_ids = api.friends.ids(screen_name=screen_name)
    pp('Following IDs', following_ids)

    source_id = follower_ids[0]
    target_id = follower_ids[1]
    friendship = api.friendships.show(source_id=source_id, target_id=target_id)
    pp('Friendship between two followers', friendship)

    # lists_all includes lists the user subscribes to AND lists ther user owns
    lists_all = api.lists.all(screen_name=screen_name)

    lists_own = api.lists(screen_name=screen_name)
    pp('Lists owned', lists_own)

    lists_own_ids = [l['id'] for l in lists_own['lists']]
    lists_follow = [l for l in lists_all if l['id'] not in lists_own_ids]
    pp('Lists follow', lists_follow)

    retweeted_to_user = api.statuses.retweeted_to_user(screen_name=screen_name)
    pp('the most recent retweets posted by users the specified user follows',
        retweeted_to_user[0])

    retweeted_by_user = api.statuses.retweeted_by_user(screen_name=screen_name)
    pp('the most recent retweets posted by the specified user',
        retweeted_by_user[0])

    user_timeline = api.statuses.user_timeline(screen_name=screen_name)

    tweet = user_timeline[0]
    pp('Latest tweet', tweet)

    # statuses/:id/retweeted_by
    retweeted_by = api.statuses(id='%s/retweeted_by' % tweet['id'])

    # following API calls only work for "me"
    retweeted_by_me = api.statuses.retweeted_by_me()
    pp('Retweeted by me', retweeted_by_me)

    retweeted_to_me = api.statuses.retweeted_to_me()
    pp('Retweeted to me', retweeted_to_me)

    retweets_of_me = api.statuses.retweets_of_me()
    pp('Retweeted of me', retweets_of_me)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print "usage: api_test.py [twiter screen name]"
        sys.exit()
    screen_name = sys.argv[1]
    test_api(screen_name)
