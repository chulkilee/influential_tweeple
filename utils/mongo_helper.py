import pymongo
import utils

MONGODB_CONF = utils.CONFIG['mongodb']
DATABASE_NAME = 'influential'

__mongodb_db = None


def get_db():
    global __mongodb_db
    if not __mongodb_db:
        conn = pymongo.Connection(**MONGODB_CONF)
        __mongodb_db = conn[DATABASE_NAME]
    return __mongodb_db


def get_tweets():
    """Return a mongodb collection for tweets"""
    db = get_db()
    collection = db['tweets']
    return collection


def get_users():
    """Return a collecton for users"""
    db = get_db()
    collection = db['users']
    return collection


def get_events():
    """Return a collecton for events"""
    db = get_db()
    collection = db['events']
    return collection


def get_keywords():
    """Return a collection for keywords"""
    db = get_db()
    collection = db['keywords']
    return collection


def get_entities():
    """Return a collection for entities"""
    db = get_db()
    collection = db['entities']
    return collection


def get_oldest_tweet(condition={}):
    # FIXME sort by created_at?
    tweets = get_tweets().find(condition,
        limit=1, sort=[('id', pymongo.ASCENDING)])
    if tweets.count():
        return tweets[0]
    else:
        return None


def get_latest_tweet(condition={}):
    # FIXME sort by created_at?
    tweets = get_tweets().find(condition,
        limit=1, sort=[('id', pymongo.DESCENDING)])
    if tweets.count():
        return tweets[0]
    else:
        return None


def get_user(screen_name):
    """Return a user document for given twitter screen name"""
    return get_users().find_one({"screen_name": screen_name})


def get_user_by_id_str(id_str):
    """Return a user document for given twitter id"""
    if isinstance(id_str, int):
        id_str = str(id_str)
    return get_users().find_one({"id_str": id_str})


def get_tweets_for_id_str(id_str):
    """Return all tweet documents for given twitter user_id"""
    return get_tweets().find({"user.id_str": id_str})


def get_tweets_for_screen_name(screen_name):
    """Return all tweet documents for given twitter screen_name"""
    return get_tweets().find({"user.screen_name": screen_name})


def get_tweets_home_timeline(id_str):
    return get_tweets().find({'r.home_timeline': id_str})


def get_keywords_for_id_str(id_str):
    "Return all keyword documents for given twitter screen_name"""
    return get_keywords().find({"user": {"id_str": id_str}})


def get_entities_for_id_str(id_str):
    """Return all entity documents for given twitter screen_name"""
    return get_entities().find({"user": {"id_str": id_str}})
