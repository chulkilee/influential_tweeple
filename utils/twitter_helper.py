import twitter
import oauth2 as oauth
import utils

AUTH_CONF = utils.CONFIG['twitter']['oauth']


def get_api_fetcher():
    """Return a instance for twitter api for fetcher"""
    api = twitter.Twitter(auth=twitter.oauth.OAuth(
        token=AUTH_CONF['oauth_token'],
        token_secret=AUTH_CONF['oauth_token_secret'],
        consumer_key=AUTH_CONF['consumer_key'],
        consumer_secret=AUTH_CONF['consumer_secret']))
    return api


def get_stream_fetcher():
    """Return a instance for twitter stream for fetcher"""
    stream = twitter.TwitterStream(secure=True, auth=twitter.oauth.OAuth(
        token=AUTH_CONF['oauth_token'],
        token_secret=AUTH_CONF['oauth_token_secret'],
        consumer_key=AUTH_CONF['consumer_key'],
        consumer_secret=AUTH_CONF['consumer_secret']))
    return stream


def get_api(oauth_token, oauth_secret):
    """Return a instance for twitter api after oauth"""
    api = twitter.Twitter(auth=twitter.oauth.OAuth(
        token=oauth_token,
        token_secret=oauth_secret,
        consumer_key=AUTH_CONF['consumer_key'],
        consumer_secret=AUTH_CONF['consumer_secret']))
    return api


def get_oauth_consumer():
    return oauth.Consumer(AUTH_CONF['consumer_key'],
        AUTH_CONF['consumer_secret'])


def get_oauth_client():
    consumer = get_oauth_consumer()
    return oauth.Client(consumer)


def get_oauth_client_with_token(oauth_token, oauth_secret):
    consumer = get_oauth_consumer()
    token = oauth.Token(oauth_token, oauth_secret)
    return oauth.Client(consumer, token)
