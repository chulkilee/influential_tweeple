import yaml
import time
import simplejson as json


CONFIG = yaml.load(file('config.yml', 'r'))


def pp(h, data, seconds=1):
    """Print heading and data as json then wait for given time (default 1s)"""
    print "\n" + h
    print "=" * len(h)
    print json.dumps(data, sort_keys=True, indent=4, ensure_ascii=False)
    print "\n"
    time.sleep(seconds)


def heading(s):
    print
    print '=' * 80
    print s
    print '-' * 80
